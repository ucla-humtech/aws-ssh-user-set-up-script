#!/bin/bash

useradd isaiah
mkdir /home/isaiah/.ssh
chmod 700 /home/isaiah/.ssh
touch /home/isaiah/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7gYBp2U9PcSuXgbK2NRiderGo4iSArPa7XuYlTkGSjYKkykBK0znIkhPnfWPUimiJ5fpSiMNFOrt1MAsaEzECJYY/oB5Ggv24XToXNUuFzXIrKY7HkVoEV8Tg1ckNcWcC28/e7l25Qw7J2TjFp+KKz3GrtOjZnJljWI7zNebenHkobeIstuZWvKqf5oyRAN2pZgV3sshL8rR+134DMYaOliCYXVKqeFb0i4xWKwXxHBt2lMoLj6tG+K+VyOrGAhw2nTuXKU2XjScmRK4Esdw8jYIUkejfRRTp4zBP4Z4+svadM/tEUppNZMLBFDVDItdcrd3V9nrbgTOr38FKlqxp inolan@ppb1075-nolan.humnet.ucla.edu" >> /home/isaiah/.ssh/authorized_keys
chmod 600 /home/isaiah/.ssh/authorized_keys
chown -R isaiah:isaiah /home/isaiah/.ssh
usermod -aG sudo isaiah 

useradd lucian
mkdir /home/lucian/.ssh
chmod 700 /home/lucian/.ssh
touch /home/lucian/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCmISbpX+uVUrBpfrZEKRLH7dqU9t3Puh0fOGdArun7em8a+KwfwgqPqHOcttbBBGBiqeRk0egFvTw2MjUXkFIK0vPbxXRkVTwQ2JE5XdAmM1KPjeF1XQdHrsHTWPhP3FWHNcXyp0vHLr4g6iPJegfYOtGIEhOHOl3JLuxRU2OjVlNwcCONzbfWfMRGvV/uGvrsCZ7z4OpcI7l22dpnnFb609meGHwuekV42TnEISNNpmMiU2ows8bbxnOnQxgW2JUQTXU+Tluwtvn7mVNJM2G9YRg4GgPV7ioNUFU9c5rkkhdCnmTnkNAcilikf3N16CpkvquB89ciHzMeLSa8zomA2VcFqSDM22wGZEN/jS6zQiyIA2SVAUeevlBQYHg2BXNkJTcv6V+8UjfVeRby3anvm7eXGsZnxFZ7EtIi7fMgrj5RNaHSAvAgniGcCTKDvoqD0UlbnDfq/dfPuQnAzVDSen8rBiz6qhS1hUZMxKpwJgXc/VhzZ0fIgeiFsLI88ek= luciantucker@PPB1085-HT-Lucian" >> /home/lucian/.ssh/authorized_keys
chmod 600 /home/lucian/.ssh/authorized_keys
chown -R lucian:lucian /home/lucian/.ssh
usermod -aG sudo lucian

useradd daniel
mkdir /home/daniel/.ssh
chmod 700 /home/daniel/.ssh
touch /home/daniel/.ssh/authorized_keys
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILCyu3EAs4UCeMpuzzRHCM/yJ/TFv02PW1GGoYiX+pCb dpark@dpark-NUC8i7BEH" >> /home/daniel/.ssh/authorized_keys
chmod 600 /home/daniel/.ssh/authorized_keys
chown -R daniel:daniel /home/daniel/.ssh
usermod -aG sudo daniel
