#!/bin/bash

useradd isaiah
mkdir /home/isaiah/.ssh
chmod 700 /home/isaiah/.ssh
touch /home/isaiah/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIVXSSt0CBDEqcDMjLpHVOEGQu63d0bIHomRxhnaW9/qpeo0uPGzdOLw2VdQpcNq6ZqLJBCqlZ1E/vkAhc+yKBjd42m2KnNUYNwUQOu3/4sgH6GWvNtUWAGJVa3JbyBd5ZrKv4tZAUDGbwJfhZ6OJhL17maH5gtIZaSDh2Y51HHxGfZOAqR1EO+uMz8KFNsQaseWMmmyPBqZT/cJiwkremT/oLhKdB4vJVRZbVxVnK74ACilX2/frnIkspPJObTPnfTAn3pmjUkpT9dzb3qzRy1jLPEaqufxR0ZXlFpAk0CJcoKJkeeOnHCPbhPe8ghlgtPUhIxtUIkY/2jEXcICct inolan@ppb1075-nolan.humnet.ucla.edu" >> /home/isaiah/.ssh/authorized_keys
chmod 600 /home/isaiah/.ssh/authorized_keys
chown -R isaiah:isaiah /home/isaiah/.ssh
usermod -aG wheel isaiah 

useradd lucian
mkdir /home/lucian/.ssh
chmod 700 /home/lucian/.ssh
touch /home/lucian/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6Fdj6f/ZGZjJbH34GJChZr9D0N3JJP3NPam57pcwznL3aJihLyYEG+uuZuS6G151krNOdpNqRteWua+2u/2fNtbHhHnWdvRLv9NQP99KWmZ0OpDqsvvkpjr6jD8eALW22tl3EgRad+CJ52aztfdQ35ZFauVA6/EXNjdetO2WTgyCrc5rSsDRtEBn8Lc0OZdM5y8zxFR0E3Iyj11WcB0tIJxd2x+gFoRZgDaGspNUSUlL0rFDSeUe0D273L+izsXUJlurJ/UzDqXsSodsYfobdYXulGk7xKE7/1mfIOWm+ZxLs5U69zNvubhoD7xOtgCGRqvXQBmLu2sON2IXH9QnGP6ia0LRviG5nL+zac1i/2LX34/CIH2/N5DFKgJy+UwEguK65q3uAyJFqtp3QpefAA8puLoU/uuTlFf73CDDWdP4vgjHgHRbuL2LQF28N2kK0Vb0ZRFZCebbHErl3EEKW4x78HU8q+5qBipNlcNPl5u7n8XQJ4KV3AbuLKy1blvU= luciantucker@PPB1085-HT-Lucian" >> /home/lucian/.ssh/authorized_keys
chmod 600 /home/lucian/.ssh/authorized_keys
chown -R lucian:lucian /home/lucian/.ssh
usermod -aG wheel lucian

useradd daniel
mkdir /home/daniel/.ssh
chmod 700 /home/daniel/.ssh
touch /home/daniel/.ssh/authorized_keys
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBPiu0yNujMGqE0GqOZMvDoE2umS6aSymSpM3W9Clysk dpark@dpark-NUC8i7BEH" >> /home/daniel/.ssh/authorized_keys
chmod 600 /home/daniel/.ssh/authorized_keys
chown -R daniel:daniel /home/daniel/.ssh
usermod -aG wheel daniel
