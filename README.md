# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	This is a repo for scripts to set up user accounts with SSH access on AWS servers.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	1. Copy one of the scripts to your new server.
	2. Execute chmod +x script.sh to give it the executable bit.
	3. Create the users with useradd usernamehere.
	4. Add the Users to the NOPASSWD sudo/wheel group with "sudo visudo".
	5. Run the scripr with ./script.sh
* Dependencies
	Users need to be created first.
